package ninja.clc.codetest.presentation.ui.main

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import junit.framework.Assert.assertEquals
import ninja.clc.codetest.data.CatRepository
import ninja.clc.codetest.domain.main.CatItemViewModel
import ninja.clc.codetest.domain.main.MainViewModel
import ninja.clc.codetest.presentation.ui.common.SchedulerProvider
import ninja.clc.codetest.presentation.ui.common.ScreenState
import ninja.clc.codetest.presentation.ui.model.CatUi
import org.junit.Before
import org.junit.Test
import javax.inject.Provider

class MainViewModelTest() {

    companion object {
        const val URL = "http://www.google.com"
    }

    private val catRepository = mockk<CatRepository>()
    private val catItemViewModelProvider = mockk<Provider<CatItemViewModel>>()
    private val catItemViewModel = mockk<CatItemViewModel>()
    private val schedulerProvider = mockk<SchedulerProvider>()
    private val cats = arrayListOf<CatUi>()
    private val cat1 = mockk<CatUi>()

    private val subject = MainViewModel(catRepository, catItemViewModelProvider, schedulerProvider)

    @Before
    fun setup() {
        every { schedulerProvider.getIoScheduler() } returns Schedulers.trampoline()
        every { schedulerProvider.getMainScheduler() } returns Schedulers.trampoline()
        every { catItemViewModelProvider.get() } returns catItemViewModel
        every { cat1.url } returns URL
        every { cat1.url } returns URL
    }

    @Test
    fun testOnCreate_loadsData() {
        every { catRepository.getPublicCats() } returns Observable.just(cats)

        subject.onCreate()

        verify(exactly = 1) { catRepository.getPublicCats() }
        assertEquals(subject.state.get(), ScreenState.DATA)
    }

    @Test(expected = Exception::class)
    fun testOnCreate_withError_showsError() {
        val error = mockk<Throwable>()
        every { catRepository.getPublicCats() } returns Observable.error(error)

        subject.onCreate()

        assertEquals(subject.state.get(), ScreenState.ERROR)
        verify(exactly = 1) { error.printStackTrace() }
    }
}