package ninja.clc.codetest.data.model

import com.google.gson.annotations.SerializedName

data class CatCategory(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String
)