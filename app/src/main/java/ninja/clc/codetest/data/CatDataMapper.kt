package ninja.clc.codetest.data

import ninja.clc.codetest.data.model.Cat
import ninja.clc.codetest.presentation.ui.model.CatUi
import javax.inject.Inject

class CatDataMapper @Inject constructor() {

    fun mapCatList(cats: List<Cat>): List<CatUi> {
        val catUiList = arrayListOf<CatUi>()
        cats.forEach {
            catUiList.add(mapCat(it))
        }
        return catUiList
    }

    fun mapCat(cat: Cat): CatUi = CatUi(cat.url)

}