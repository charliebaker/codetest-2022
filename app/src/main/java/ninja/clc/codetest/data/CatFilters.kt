package ninja.clc.codetest.data

import ninja.clc.codetest.data.model.Cat
import javax.inject.Inject

class CatFilters @Inject constructor() {

    companion object {
        val FILTERED_CATEGORIES = listOf("hats")
    }

    fun isFiltered(cat: Cat): Boolean {
        cat.categories?.let { catCategories -> catCategories.forEach {
            if (it.name in FILTERED_CATEGORIES) {
                return false
            }
        }}
        return true
    }
}