package ninja.clc.codetest.data

import dagger.Module
import dagger.Provides

@Module
class DataModule {

    @Provides
    fun provideRepository(repository: CatRepositoryImpl): CatRepository {
        return repository
    }

}