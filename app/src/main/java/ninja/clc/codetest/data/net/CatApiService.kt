package ninja.clc.codetest.data.net

import io.reactivex.rxjava3.core.Observable
import ninja.clc.codetest.data.model.Cat
import retrofit2.http.GET
import retrofit2.http.Query


interface CatApiService {

    @GET("images/search")
    fun searchPublicImages(
        @Query("mime_types") mime_types: String = "gif",
        @Query("order") order: String = "random",
        @Query("limit") limit: Int = 5
    ): Observable<List<Cat>>
}