package ninja.clc.codetest.data

import io.reactivex.rxjava3.core.Observable
import ninja.clc.codetest.data.net.CatApiService
import ninja.clc.codetest.presentation.ui.model.CatUi
import javax.inject.Inject

class CatRepositoryImpl @Inject constructor(
    private val catApiService: CatApiService,
    private val filters: CatFilters,
    private val mapper: CatDataMapper
): CatRepository {

    override fun getPublicCats(): Observable<List<CatUi>> {
        return catApiService.searchPublicImages()
            .map {
                it.filter { cat -> filters.isFiltered(cat) }
            }
            .map { mapper.mapCatList(it) }
    }
}