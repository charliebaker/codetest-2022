package ninja.clc.codetest.data.model

import com.google.gson.annotations.SerializedName

data class Cat(
    @SerializedName("categories") val categories: List<CatCategory>?,
    @SerializedName("id") val id: String,
    @SerializedName("url")  val url: String
)