package ninja.clc.codetest.data

import io.reactivex.rxjava3.core.Observable
import ninja.clc.codetest.presentation.ui.model.CatUi

interface CatRepository {

    fun getPublicCats(): Observable<List<CatUi>>

}