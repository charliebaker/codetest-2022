package ninja.clc.codetest.domain.main

import androidx.databinding.ObservableField
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import ninja.clc.codetest.presentation.ui.common.BaseViewModel
import ninja.clc.codetest.presentation.ui.model.CatUi
import javax.inject.Inject

class CatDetailViewModel @Inject constructor() : BaseViewModel() {

    val image = ObservableField<String>()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun bindOnResume() {

    }

    fun init(data: CatUi?) {
        data?.let {
            image.set(data.url)
        }
    }
}