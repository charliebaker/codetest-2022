package ninja.clc.codetest.domain.main

import androidx.databinding.ObservableField
import androidx.lifecycle.*
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ninja.clc.codetest.data.CatRepository
import ninja.clc.codetest.data.model.Cat
import ninja.clc.codetest.presentation.ui.common.BaseViewModel
import ninja.clc.codetest.presentation.ui.common.SchedulerProvider
import ninja.clc.codetest.presentation.ui.common.ScreenState
import ninja.clc.codetest.presentation.ui.common.SingleLiveEvent
import ninja.clc.codetest.presentation.ui.main.adapter.MainCatRecyclerAdapter
import ninja.clc.codetest.presentation.ui.model.CatUi
import javax.inject.Inject
import javax.inject.Provider

class MainViewModel @Inject constructor(
    private val catRepository: CatRepository,
    private val catItemViewModelProvider: Provider<CatItemViewModel>,
    private val schedulerProvider: SchedulerProvider
) : BaseViewModel() {

    val state = ObservableField(ScreenState.LOADING)
    val adapter = ObservableField<MainCatRecyclerAdapter>()
    val startDetailsListener = SingleLiveEvent<CatUi>()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        loadData()
    }

    fun onRetryClick() {
        loadData()
    }

    private fun loadData() {
        compositeSubscription.add(catRepository.getPublicCats()
            .subscribeOn(schedulerProvider.getIoScheduler())
            .observeOn(schedulerProvider.getMainScheduler())
            .doOnSubscribe { state.set(ScreenState.LOADING) }
            .subscribe({ handleData(it) }, { handleError(it) }))
    }

    private fun handleData(cats: List<CatUi>) {
        // TODO: Improvement - View for empty data.
        val catItemViewModels: ArrayList<CatItemViewModel> = arrayListOf()
        cats.forEach {
            val viewModel = catItemViewModelProvider.get()
            viewModel.setData(it, onClick)
            catItemViewModels.add(viewModel)
        }
        adapter.set(MainCatRecyclerAdapter(catItemViewModels))
        state.set(ScreenState.DATA)
    }

    val onClick: (cat: CatUi) -> Unit = {
        startDetailsListener.value = it
    }

    private fun handleError(throwable: Throwable) {
        // TODO: Would do something more valuable than this in a real app.
        state.set(ScreenState.ERROR)
        throwable.printStackTrace()
    }
}