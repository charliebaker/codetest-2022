package ninja.clc.codetest.domain.main

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import ninja.clc.codetest.presentation.ui.model.CatUi
import javax.inject.Inject

class CatItemViewModel @Inject constructor(): ViewModel() {

    private lateinit var cat: CatUi
    val image = ObservableField<String>()
    private var listener: (CatUi) -> Unit = { }

    fun setData(cat: CatUi, onClick: (cat: CatUi) -> Unit) {
        this.cat = cat
        image.set(cat.url)
        listener = onClick
    }

    fun onImageClick() {
        listener.invoke(cat)
    }
}