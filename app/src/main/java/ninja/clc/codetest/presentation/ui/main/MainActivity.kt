package ninja.clc.codetest.presentation.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ninja.clc.codetest.databinding.ActivityMainBinding
import ninja.clc.codetest.domain.main.MainViewModel
import ninja.clc.codetest.presentation.ui.common.BaseActivity
import ninja.clc.codetest.presentation.ui.detail.CatDetailActivity
import ninja.clc.codetest.presentation.ui.model.CatUi
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        getComponent().inject(this)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.setLifecycle(lifecycle)

        binding.containerDataRecyclerview.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        bindObservers()
    }

    private fun bindObservers() {
        viewModel.startDetailsListener.observe(this, { launchDetailScreen(it) })
    }

    private fun launchDetailScreen(cat: CatUi) {
        startActivity(CatDetailActivity.getLaunchIntent(this, cat))
    }
}