package ninja.clc.codetest.presentation.ui.model

import android.os.Parcel
import android.os.Parcelable

data class CatUi(
    val url: String?
) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CatUi> {
        override fun createFromParcel(parcel: Parcel): CatUi {
            return CatUi(parcel)
        }

        override fun newArray(size: Int): Array<CatUi?> {
            return arrayOfNulls(size)
        }
    }
}