package ninja.clc.codetest.presentation.ui.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.HasAndroidInjector
import ninja.clc.codetest.presentation.CodeTestApplication

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun getComponent() = (application as CodeTestApplication).component
}