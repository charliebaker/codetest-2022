package ninja.clc.codetest.presentation.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ninja.clc.codetest.R
import ninja.clc.codetest.domain.main.CatItemViewModel
import ninja.clc.codetest.presentation.ui.common.CommonViewModelHolder
import ninja.clc.codetest.presentation.ui.model.CatUi

class MainCatRecyclerAdapter constructor(
    private val catItemViewModels: List<CatItemViewModel> = emptyList(),
): RecyclerView.Adapter<CommonViewModelHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommonViewModelHolder {
        return CommonViewModelHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_cat, parent, false))
    }

    override fun onBindViewHolder(holder: CommonViewModelHolder, position: Int) {
        holder.bind(catItemViewModels[position])
    }

    override fun getItemCount(): Int {
        return catItemViewModels.size
    }
}