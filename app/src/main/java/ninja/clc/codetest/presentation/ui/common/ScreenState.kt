package ninja.clc.codetest.presentation.ui.common

enum class ScreenState {
    LOADING,
    DATA,
    ERROR
}