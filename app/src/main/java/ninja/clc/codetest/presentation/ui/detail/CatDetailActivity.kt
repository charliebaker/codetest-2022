package ninja.clc.codetest.presentation.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import ninja.clc.codetest.databinding.ActivityCatDetailBinding
import ninja.clc.codetest.domain.main.CatDetailViewModel
import ninja.clc.codetest.presentation.ui.common.BaseActivity
import ninja.clc.codetest.presentation.ui.model.CatUi
import javax.inject.Inject

class CatDetailActivity : BaseActivity() {

    @Inject lateinit var viewModel: CatDetailViewModel
    private lateinit var binding: ActivityCatDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        getComponent().inject(this)
        super.onCreate(savedInstanceState)

        binding = ActivityCatDetailBinding.inflate(layoutInflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.init(getData())
        viewModel.setLifecycle(lifecycle)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
    }

    private fun getData(): CatUi? {
        val cat = intent.extras?.getParcelable<CatUi>(EXTRA_CAT)
        return cat
    }

    companion object {
        private const val EXTRA_CAT = "extra_cat"

        fun getLaunchIntent(context: Context, cat: CatUi): Intent {
            val launchIntent = Intent(context, CatDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable(EXTRA_CAT, cat)
            launchIntent.putExtras(bundle)
            return launchIntent
        }
    }

}