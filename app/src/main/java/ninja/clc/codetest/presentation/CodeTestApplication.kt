package ninja.clc.codetest.presentation

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import ninja.clc.codetest.presentation.di.AppComponent
import ninja.clc.codetest.presentation.di.DaggerAppComponent
import javax.inject.Inject

class CodeTestApplication : Application() {

    lateinit var component: AppComponent

    override fun onCreate() {
        component = DaggerAppComponent.create()
        super.onCreate()
    }
}