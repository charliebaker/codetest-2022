package ninja.clc.codetest.presentation.di

import dagger.Module
import dagger.Provides
import ninja.clc.codetest.data.net.CatApiService
import ninja.clc.codetest.data.net.CatAuthenticationInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module
class NetworkModule {

    @Provides
    @Named("apiKey")
    fun provideApiKey(): String {
        return "03a70baa-bbd9-492b-bd1c-13710822c7de"
    }

    @Provides
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return "https://api.thecatapi.com/v1/"
    }

    @Provides
    fun provideRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
    }

    @Provides
    fun provideRetrofit(
        builder: Retrofit.Builder,
        @Named("baseUrl") baseUrl: String,
        client: OkHttpClient): Retrofit {

        return builder
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    @Provides
    fun provideHttpClient(
        @Named("apiKey") apiKey: String,
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(CatAuthenticationInterceptor(apiKey))
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    fun provideLogging(): HttpLoggingInterceptor {
        val logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BODY
        return logger
    }

    @Provides
    fun provideCatApiService(retrofit: Retrofit): CatApiService {
        return retrofit.create(CatApiService::class.java)
    }
}