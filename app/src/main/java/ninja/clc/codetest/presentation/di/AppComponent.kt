package ninja.clc.codetest.presentation.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import ninja.clc.codetest.data.DataModule
import ninja.clc.codetest.presentation.ui.common.BaseActivity
import ninja.clc.codetest.presentation.ui.main.MainActivity
import ninja.clc.codetest.domain.main.MainViewModel
import ninja.clc.codetest.presentation.ui.detail.CatDetailActivity

@Component(modules = [NetworkModule::class, DataModule::class])
interface AppComponent {

    fun inject(activity: MainActivity)
    fun inject(viewModel: MainViewModel)
    fun inject(catDetailActivity: CatDetailActivity)
}