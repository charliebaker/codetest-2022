package ninja.clc.codetest.presentation.ui.common

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import ninja.clc.codetest.BR

class CommonViewModelHolder constructor(private val dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root) {

    fun <VM> bind(viewModel: VM) {
        dataBinding.setVariable(BR.viewModel, viewModel)
        dataBinding.executePendingBindings()
    }
}