# Decisions:

 - Uses MVVM architecture using Databinding
 - Use ViewModels for easy testing.
 - Split out the project into "presentation", "data" and "domain" modules to mimic clean architecture, did not have time to create the actual modules.
 - Uses existing libraries that are well used, to quickly build the code test.
 - Did not split out some of the sections of code into separate modules due to time constraints.
 - Uses git-flow workflow (master for active code, release for releases, develop for dev and feature branches.)

# Improvements:

 - Use updated lifecycle component functions
 - Views for empty data when successful
 - Use a more repository pattern repository pattern to hide the source of data.
 - Use JetPack compose as we have only just started to use this at Ford and beginning to learn.
 - Probably a lot more that could be done to improve given more time.